From node:carbon as node
WORKDIR /app
COPY package*.json /app/
RUN npm install
COPY ./ /app/
arg TARGET=ng-deploy-dev
RUN npm run ${TARGET}

From nginx:1.13
copy --from=node /app/dist/ /usr/share/nginx/html
copy ./nginx-custom.conf /etc/nginx/conf.d/default.conf

expose 80